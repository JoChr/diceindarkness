using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterProperties : MonoBehaviour
{
    [Header("Health Settings")]
    [SerializeField] private int MaxHealth = 2000;
    [SerializeField] private int RegenerateAmount = 1;

    [Header("Speed Settings")]
    [SerializeField] public float MoveSpeed = 20f;
    [SerializeField] private float RotationSpeed = 100f;
    [SerializeField] private int Angle = 6;
    [SerializeField] private float SphereForce = 200;
    [SerializeField] private int RightClickSpeed = 25;
    [SerializeField] private int LeftClickSpeed = 15;

    [Header("Damage Settings")]
    [SerializeField] private int SpinSelfDamage = 1;
    [SerializeField] private int NormalDamage = 200;
    [SerializeField] private int BoostedDamage = 400;
    [SerializeField] private int MushroomDamage = 100;
    [SerializeField] private int ShieldDamage = 100;

    [Space]
    public Animator ScoreboardAnimator;

    public static int maxHealth = 2000;
    public static int regenerateAmount = 1;


    public static float moveSpeed = 20f;
    public static float rotationSpeed = 100f;
    public static int angle = 6;
    public static float sphereForce = 200;
    public static int rightClickSpeed = 25;
    public static int leftClickSpeed = 15;


    public static int spinSelfDamage = 1;
    public static int normalDamage = 200;
    public static int boostedDamage = 400;
    public static int mushroomDamage = 100;
    public static int shieldDamage = 100;

    public static Animator scoreboardAnimator;

    private void Start()
    {
        maxHealth = MaxHealth;
        regenerateAmount = RegenerateAmount;

        moveSpeed = MoveSpeed;
        rotationSpeed = RotationSpeed;
        angle = Angle;
        sphereForce = SphereForce;
        rightClickSpeed = RightClickSpeed;
        leftClickSpeed = LeftClickSpeed;


        spinSelfDamage = SpinSelfDamage;
        normalDamage = NormalDamage;
        boostedDamage = BoostedDamage;
        mushroomDamage = MushroomDamage;
        shieldDamage = ShieldDamage;

        scoreboardAnimator = ScoreboardAnimator;
    }

}
