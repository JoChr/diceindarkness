using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class GameLength : MonoBehaviourPun
{

    public float timer;
    public static float gameLength;
    public static bool timeStarted = false;
    public static bool gameEnd = false;
    public TMP_Text timerText;
    private bool gameHasEnded = false;
    private SoundController soundController;
    private void Awake()
    {
        gameLength = timer;
        gameEnd = false;
    }
    private void Start()
    {
        soundController = FindObjectOfType<SoundController>();
    }
    void OnGUI()
    {
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        timerText.text = niceTime;
        //GUI.Label(new Rect(10, 100, 250, 100), niceTime);
        Debug.Log(niceTime);
    }
    // Update is called once per frame
    void Update()
    {
        

        if (PhotonNetwork.IsMasterClient)
        {
            if (timeStarted == true && !gameEnd)
            {
                timer -= Time.deltaTime;
            }
        
            UpdateTime(timer);
            gameObject.GetPhotonView().RPC("UpdateTime", RpcTarget.Others, timer);

        }


        if (gameEnd && gameHasEnded)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                foreach (var item in GameObject.FindGameObjectsWithTag("Player"))
                {
                    PhotonNetwork.Destroy(item);
                }
                PhotonNetwork.LoadLevel("EndGame");
                soundController.Stop("GameMusic");
                gameHasEnded = false;
                return;
            }

            
        }

    }



    [PunRPC]
    public void UpdateTime(float time)
    {
        timer = time;

        if (time <= 0)
        {
            timer = gameLength;
            gameEnd = true;
            gameHasEnded = true;
        }
    }

    
}
