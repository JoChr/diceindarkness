using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Photon.Pun;
using UnityEngine.UI;
using System.Text;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviourPun, IPunObservable
{
    //[SerializeField] private float movementSpeed;
    public int maxHealth = 2000;
    public int currentHealth = 2000;
    private int spinDamage = CharacterProperties.spinSelfDamage;
    public bool morePower = false;
    public bool isShielded = false;
    private Animator animator;
    private Animator deathAnimator;
    private int skinNumber;

    private SoundController soundController;
    private UpdateScoreboard updateScoreboard;

    public void SetAnimator(RuntimeAnimatorController controller)
    {
        animator.runtimeAnimatorController = controller;
    }

    public static GameObject LocalPlayerInstance;

    [Space]
    private float moveSpeed = CharacterProperties.moveSpeed;
    private float rotationSpeed = CharacterProperties.rotationSpeed;
    private int angle = CharacterProperties.angle;
    private float sphereForce = CharacterProperties.sphereForce;

    [Space]
    public HealthBar healthBar;
    [SerializeField] private GameObject sphere;
    [SerializeField] private VisualEffect heal;
    [SerializeField] private GameObject dust;


    private GameObject minimapCamera;
    private Plane plane = new Plane(Vector3.up, 0);
    private Rigidbody sphereRig;
    private Rigidbody playerRig;

    public PhotonView view;
    public bool isDead = false;
    private int oldHealth;

    public bool followMouse = true;




    private Vector3 rayDistance;
    //scoreboard
    GameObject scoreboard;
    public List<PhotonPlayer> sortedplayerList;

    [SerializeField] GameObject[] skins;
    private bool firstInput = true;

    private void Awake()
    {
        //Photon View, checken ob es die eigene View Component ist
        view = GetComponent<PhotonView>();

        if (view.IsMine)
        {
            PlayerController.LocalPlayerInstance = this.gameObject;
        }
        DontDestroyOnLoad(this.gameObject);


        minimapCamera = GameObject.FindGameObjectWithTag("MapCamera").gameObject;



        try
        {
            animator = GetComponent<Animator>();


        }
        catch (System.Exception)
        {
            Debug.Log("Kein Animator");
            throw;
        }



    }
    // Start is called before the first frame update
    void Start()
    {
        sphereRig = sphere.GetComponent<Rigidbody>();
        playerRig = gameObject.GetComponent<Rigidbody>();
        maxHealth = 2000;
        //HealthBar
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        isDead = false;
        oldHealth = currentHealth;
        moveSpeed = CharacterProperties.moveSpeed;
        spinDamage = CharacterProperties.spinSelfDamage;
        rotationSpeed = CharacterProperties.rotationSpeed;
        angle = CharacterProperties.angle;
        sphereForce = CharacterProperties.sphereForce;
        PhotonNetwork.SendRate = 60;
        PhotonNetwork.SerializationRate = 30;
        PhotonNetwork.AutomaticallySyncScene = true;
        soundController = FindObjectOfType<SoundController>();
        updateScoreboard = FindObjectOfType<UpdateScoreboard>();

        //Checken obs die eigene Cam ist, wenn ja followen
        CameraFollow cameraFollow = gameObject.GetComponent<CameraFollow>();


        if (cameraFollow != null)
        {
            if (view.IsMine)
            {
                cameraFollow.OnStartFollowing();
            }

        }
        else
        {
            Debug.LogError("CameraFollow fehlt");
        }

        if (view.IsMine)
        {

            //scoreboard
            scoreboard = GameObject.Find("Canvas-GameLength").transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
        }
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isDead && !GameLength.gameEnd && MapGenerator.playersLoaded)
        {
            handleInput();
        }
    }

    private void Update()
    {
        if (!GameLength.gameEnd && MapGenerator.playersLoaded)
        {
            if (view.IsMine)
            {

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    followMouse = !followMouse;
                }

                if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
                {
                    //TODO: Stop Heal Sound
                    soundController.Stop("HealSound");
                    SetMoveSpeed(CharacterProperties.moveSpeed);
                    heal.enabled = false;
                }

                if (Input.GetMouseButtonDown(1))
                {

                    firstInput = false;

                    if (currentHealth > spinDamage)
                    {
                        SetMoveSpeed(CharacterProperties.rightClickSpeed);
                        //GetComponent<Animator>().SetTrigger("isFighting");
                        GetComponent<Animator>().SetBool("fight", true);

                    }


                }

                if (Input.GetMouseButtonUp(1))
                {

                    firstInput = false;

                    GetComponent<Animator>().SetBool("fight", false);

                    GetComponent<Animator>().Play("0_Walk", 0);

                }

            }


            if (view.IsMine)
            {
                if (Input.GetMouseButton(0) && !Input.GetMouseButton(1))
                {
                    firstInput = false;
                    view.RPC("Regenerate", RpcTarget.All, CharacterProperties.regenerateAmount);
                }
            }

            if (view.IsMine && PhotonNetwork.playerList.Length > 0 && MapGenerator.playersLoaded)
            {
                // && (currentHealth < oldHealth || currentHealth > oldHealth)
                view.RPC("UpdateHealth", RpcTarget.All, currentHealth);
                oldHealth = currentHealth;
            }

            if (view.IsMine)
            {


                //scoreboard
                if (Input.GetKeyDown(KeyCode.Tab))

                {
                    //TODO: PLay Papier ausroll sound

                    //scoreboard.SetActive(true);

                    CharacterProperties.scoreboardAnimator.ResetTrigger("closeScoreboard");

                    CharacterProperties.scoreboardAnimator.SetTrigger("openScoreboard");

                }
                if (Input.GetKey(KeyCode.Tab))
                {
                    CharacterProperties.scoreboardAnimator.gameObject.GetComponentInChildren<UpdateScoreboard>().SetText();
                }
                if (Input.GetKeyUp(KeyCode.Tab))
                {
                    CharacterProperties.scoreboardAnimator.ResetTrigger("openScoreboard");

                    CharacterProperties.scoreboardAnimator.SetTrigger("closeScoreboard");
                }

                if (Input.GetKeyUp(KeyCode.A))
                {
                    Die();
                }
            }
        }





    }



    [PunRPC]
    public void UpdateHealth(int health)
    {
        healthBar.SetHealth(health);
    }


    public void TakeDamage(int damage)
    {
        if (damage > 10)
        {
            //TODO PLay Hurt Sound
            int skin = (int)PhotonNetwork.player.CustomProperties["skin"];
            soundController.Play("HurtSound" + skin);
        }

        view.RPC("Damage", RpcTarget.All, damage);

    }

    [PunRPC]
    public void Damage(int damage)
    {

        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Die();
        }

    }




    public void Die()
    {

        //death = Instantiate(skins[(int)PhotonNetwork.player.CustomProperties["skin"]], gameObject.transform.position, gameObject.transform.rotation);

        if (view.IsMine)
        {

            skinNumber = (int)PhotonNetwork.player.CustomProperties["skin"];

            PhotonNetwork.Instantiate(skins[skinNumber].name, gameObject.transform.position, gameObject.transform.rotation);
            isDead = true;

            GameObject.Find("DeathCam").GetComponent<Camera>().enabled = true;
            GameObject.Find("SpawnPlayers").GetComponent<SpawnPlayers>().Respawn();

            PhotonNetwork.Destroy(gameObject);
        }


    }



    [PunRPC]
    private void Regenerate(int life)
    {
        if (currentHealth < maxHealth)
        {

            soundController.Play("HealSound");

            currentHealth += life;
            healthBar.SetHealth(currentHealth);
            SetMoveSpeed(CharacterProperties.leftClickSpeed);
            heal.enabled = true;
        }

    }

    private void SetMoveSpeed(float speed)
    {
        moveSpeed = speed;
    }




    private void handleInput()
    {

        if (view.IsMine)
        {
            float _hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (plane.Raycast(ray, out _hit) && followMouse)
            {
                Debug.DrawLine(transform.position, ray.GetPoint(_hit), Color.cyan);

                rayDistance = ray.GetPoint(_hit) - transform.position;

                if (rayDistance.magnitude > 1 && !firstInput)
                {
                    GetComponent<Animator>().ResetTrigger("isIdle");
                    if (!Input.GetMouseButton(1))
                    {
                        GetComponent<Animator>().Play("0_Walk", 0);
                    }
                    dust.SetActive(true);

                    playerRig.AddForce( 
                        rayDistance.normalized * moveSpeed,
                        ForceMode.Impulse
                        );
                }
                else
                {
                    if (!Input.GetMouseButton(1))
                    {
                        dust.SetActive(false);

                        GetComponent<Animator>().SetTrigger("isIdle");
                    }
                }

                if (Input.GetMouseButton(1))
                {
                    if (currentHealth > spinDamage * 30)
                    {
                        transform.Rotate(Vector3.up, -angle * Time.fixedDeltaTime * rotationSpeed);
                        sphereRig.AddForce(transform.right * sphereForce);

                        TakeDamage(spinDamage);
                    }
                }

                Vector3 mouseDirection = new Vector3(
                    ray.GetPoint(_hit).x,
                    transform.position.y, 
                    ray.GetPoint(_hit).z) - transform.position;

                transform.forward = Vector3.Lerp(
                    transform.forward, 
                    mouseDirection.normalized * 5,
                    Time.deltaTime * 1.5f
                    );
            }

        }

    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (MapGenerator.playersLoaded)
        {

            if (stream.IsWriting)
            {

                stream.SendNext(transform.position);
                stream.SendNext(transform.rotation);
                stream.SendNext(transform.forward);
                stream.SendNext(currentHealth);
                stream.SendNext(heal.enabled);
                stream.SendNext(animator.GetBool("fight"));


            }
            else
            {
                transform.position = (Vector3)stream.ReceiveNext();
                transform.rotation = (Quaternion)stream.ReceiveNext();
                transform.forward = (Vector3)stream.ReceiveNext();
                currentHealth = (int)stream.ReceiveNext();
                heal.enabled = (bool)stream.ReceiveNext();
                animator.SetBool("fight", (bool)stream.ReceiveNext());

            }
        }

    }



}
