using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class ClassHandler : MonoBehaviour
{
    private int skinNumber = PhotonNetwork.player.CustomProperties["skin"] != null ? (int) PhotonNetwork.player.CustomProperties["skin"]: 0;
    private void Start()
    {
        Hashtable hasch = new Hashtable();
        hasch.Add("skin", skinNumber);
        PhotonNetwork.player.SetCustomProperties(hasch);
    }
    public void ClassSelector(string className)
    {
        if (DelayStartWaitingRoomController.timerToStartGame > 1f)
        {


            switch (className)
            {
                case "Knight":
                    skinNumber = 0;
                    break;
                case "Dwarf":
                    skinNumber = 1;
                    break;
                case "Barbarian":
                    skinNumber = 2;
                    break;
                case "Wizard":
                    skinNumber = 3;
                    break;
                default:
                    skinNumber = 0;
                    break;
            }
        
            Hashtable hasch = new Hashtable();
            hasch.Add("skin", skinNumber);
            PhotonNetwork.player.SetCustomProperties(hasch);
        }
    }



}
