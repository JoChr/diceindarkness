using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraController : MonoBehaviour
{
    
    public Transform target;
    [SerializeField] private Vector3 targetOffset;
    [SerializeField] private float movementspeed;
    void Start()
    {
    }

    private void Update()
    {
       
      
        
    }
    // Update is called once per frame
    void LateUpdate()
    {
        MoveCamera();

    }

    void MoveCamera()
    {
        transform.position = Vector3.Lerp(transform.position,target.position + targetOffset,movementspeed * Time.deltaTime);
    }
}
